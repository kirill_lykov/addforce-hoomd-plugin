# Copyright (c) 2009-2017 The Regents of the University of Michigan
# This file is part of the HOOMD-blue project, released under the BSD 3-Clause License.

# this simple python interface just actiavates the c++ AddForceUpdater from cppmodule
# Check out any of the python code in lib/hoomd-python-module/hoomd_script for moreexamples

# First, we need to import the C++ module. It has the same name as this module (dpd_cell_flow) but with an underscore
# in front
from hoomd.dpd_cell_flow import _dpd_cell_flow

import hoomd
from hoomd import _hoomd

## Adds constant force to every particle in a group in the specified box
#
#
class add_force(hoomd.update._updater):
    ## Adds force
    #
    # \param period Force will be added every \a period time steps
    #
    # \b Examples:
    # \code
    # dpd_cell_flow.update.add_force(1, group.all(), force, lo, hi)
    # \endcode
    #
    # \a period can be a function: see \ref variable_period_docs for details
    def __init__(self, group, force_inc, lo, hi):
        hoomd.util.print_status_line();

        # initialize base class
        hoomd.update._updater.__init__(self);
        f = _hoomd.make_scalar3(force_inc[0], force_inc[1], force_inc[2])
        l = _hoomd.make_scalar3(lo[0], lo[1], lo[2])
        h =  _hoomd.make_scalar3(hi[0], hi[1], hi[2])

        # initialize the reflected c++ class
        if not hoomd.context.exec_conf.isCUDAEnabled():
            self.cpp_updater = _dpd_cell_flow.AddForceUpdater(hoomd.context.current.system_definition, group.cpp_group, f, l, h);
        else:
            self.cpp_updater = _dpd_cell_flow.AddForceUpdaterGPU(hoomd.context.current.system_definition, group.cpp_group, f, l, h);

        self.setupUpdater(1); # every timestep
