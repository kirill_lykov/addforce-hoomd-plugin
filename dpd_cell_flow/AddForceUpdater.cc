// Copyright (c) 2009-2017 The Regents of the University of Michigan
// This file is part of the HOOMD-blue project, released under the BSD 3-Clause License.

#include "AddForceUpdater.h"
#ifdef ENABLE_CUDA
#include "AddForceUpdater.cuh"
#endif

/*! \file AddForceUpdater.cc
  \brief Definition of AddForceUpdater
 */

// ********************************
// here follows the code for AddForceUpdater on the CPU

/*! \param sysdef System to zero the velocities of
 */
AddForceUpdater::AddForceUpdater(std::shared_ptr<SystemDefinition> sysdef, std::shared_ptr<ParticleGroup> group, Scalar3 force_inc, Scalar3 domainlo, Scalar3 domainhi)
    : Updater(sysdef), m_group(group), m_force_inc(force_inc), m_domainlo(domainlo), m_domainhi(domainhi)
{
    
}

bool AddForceUpdater::isInsideBox(const Scalar4& point) const 
{
    if (point.x >= m_domainlo.x && point.x <= m_domainhi.x
       && point.y >= m_domainlo.y && point.y <= m_domainhi.y
       && point.z >= m_domainlo.z && point.z <= m_domainhi.z)
        return true;
    return false;
}

/*! Perform the needed calculations to zero the system's velocity
  \param timestep Current time step of the simulation
 */
void AddForceUpdater::update(unsigned int timestep)
{
    if (m_prof) m_prof->push("AddForceUpdater");

    // access the particle data for writing on the CPU
    assert(m_pdata);
    ArrayHandle<Scalar4> h_pos(m_pdata->getPositions(), access_location::host, access_mode::read);
    ArrayHandle<Scalar4> h_vel(m_pdata->getVelocities(), access_location::host, access_mode::read);
    ArrayHandle<Scalar3> h_acc(m_pdata->getAccelerations(), access_location::host, access_mode::readwrite);

    for (unsigned int i = 0; i < m_group->getNumMembers(); ++i)
    {
        // get the index for the current group member
        unsigned int idx = m_group->getMemberIndex(i);
        if (isInsideBox(h_pos.data[idx])) {
            Scalar mass = h_vel.data[idx].w; 
            h_acc.data[idx].x += m_force_inc.x / mass;
            h_acc.data[idx].y += m_force_inc.y / mass;
            h_acc.data[idx].z += m_force_inc.z / mass;
        }
    }
    if (m_prof) m_prof->pop();
}

/* Export the CPU updater to be visible in the python module
 */
void export_AddForceUpdater(pybind11::module& m)
{
    pybind11::class_<AddForceUpdater, std::shared_ptr<AddForceUpdater> >(m, "AddForceUpdater", pybind11::base<Updater>())
        .def(pybind11::init<std::shared_ptr<SystemDefinition>, std::shared_ptr<ParticleGroup>, Scalar3, Scalar3, Scalar3 >())
        ;
}

// ********************************
// here follows the code for AddForceUpdater on the GPU

#ifdef ENABLE_CUDA

/*! \param sysdef System to zero the velocities of
 */
AddForceUpdaterGPU::AddForceUpdaterGPU(std::shared_ptr<SystemDefinition> sysdef, std::shared_ptr<ParticleGroup> group, Scalar3 force_inc, Scalar3 domainlo, Scalar3 domainhi)
    : AddForceUpdater(sysdef, group, force_inc, domainlo, domainhi)
{
}


/*! Perform the needed calculations to zero the system's velocity
  \param timestep Current time step of the simulation
 */
void AddForceUpdaterGPU::update(unsigned int timestep)
{
    if (m_prof) m_prof->push("AddForceUpdater");

    // access the particle data arrays for writing on the GPU
    ArrayHandle<Scalar4> d_vel(m_pdata->getVelocities(), access_location::device, access_mode::readwrite);

    // call the kernel devined in AddForceUpdater.cu
    gpu_zero_velocities(d_vel.data, m_pdata->getN());

    // check for error codes from the GPU if error checking is enabled
    if(m_exec_conf->isCUDAErrorCheckingEnabled())
        CHECK_CUDA_ERROR();

    if (m_prof) m_prof->pop();
}

/* Export the GPU updater to be visible in the python module
 */
void export_AddForceUpdaterGPU(pybind11::module& m)
{
    pybind11::class_<AddForceUpdaterGPU, std::shared_ptr<AddForceUpdaterGPU> >(m, "AddForceUpdaterGPU", pybind11::base<AddForceUpdater>())
        .def(pybind11::init<std::shared_ptr<SystemDefinition>, std::shared_ptr<ParticleGroup>, Scalar3, Scalar3, Scalar3 >())
        ;
}

#endif // ENABLE_CUDA
