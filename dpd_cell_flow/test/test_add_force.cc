// Copyright (c) 2009-2017 The Regents of the University of Michigan
// This file is part of the HOOMD-blue project, released under the BSD 3-Clause License.


// this include is necessary to get MPI included before anything else to support intel MPI
#include "hoomd/ExecutionConfiguration.h"


#include <iostream>

#include <functional>

#include "hoomd/dpd_cell_flow/AddForceUpdater.h"

#include <stdio.h>

#include "hoomd/Initializers.h"
#include "hoomd/SnapshotSystemData.h"

using namespace std;
using namespace std::placeholders;

#include "hoomd/test/upp11_config.h"
HOOMD_UP_MAIN();

typedef SystemDefinitionPtr SystemDefinitionPtr;
typedef std::shared_ptr<AddForceUpdater> AddForceUpdaterPtr;
typedef std::shared_ptr<ExecutionConfiguration> ExecutionConfigurationPtr;
//! Typedef to make using the std::function factory easier
typedef std::function<std::shared_ptr<AddForceUpdater> (SystemDefinitionPtr sysdef, std::shared_ptr<ParticleGroup> group, Scalar3 force_inc, Scalar3 domainlo, Scalar3 domainhi)> addforce_creator;

//! Perform some simple functionality tests of any AngleForceCompute
void add_force_basic_tests(addforce_creator af_creator, ExecutionConfigurationPtr exec_conf)
{
    /////////////////////////////////////////////////////////
    // start with the simplest possible test: 3 particles in a huge box
    BoxDim domain(1000.0);    
    SystemDefinitionPtr sysdef_3(new SystemDefinition(3, domain, 1, 1, 1, 0, 0,  exec_conf));
    std::shared_ptr<ParticleData> pdata_3 = sysdef_3->getParticleData();
    ArrayHandle<Scalar4> h_acc(pdata->getAccelerations(), access_location::host, access_mode::readwrite);

    std::shared_ptr<ParticleSelector> selector_all_3(new ParticleSelectorTag(sysdef, 0, pdata_3->getN()-1));
    std::shared_ptr<ParticleGroup> group_all_3(new ParticleGroup(sysdef_3, selector_all_3));

    pdata_3->setPosition(0,make_scalar3(1.0,0.0,0.0));
    pdata_3->setPosition(1,make_scalar3(0.0,1.0,0.0)); 
    pdata_3->setPosition(2,make_scalar3(0.0,0.0,1.0)); 

    // 1. Add 0 force and check that nothing has changed
    auto fc_3 = af_creator(sysdef_3, group_all_3, make_scalar3(0.0, 0.0, 0.0), domain.getLo(), domain.getHi());
    fc_3->update();
    
    {
        for (int i = 0i i < 3; ++i) {
            UP_ASSERT_EQUAL(h_acc.data[i].x, 0.0);
            UP_ASSERT_EQUAL(h_acc.data[i].y, 0.0);
            UP_ASSERT_EQUAL(h_acc.data[i].z, 0.0);
        }
    }

}


//! AddForceUpdater creator for add_force_basic_tests()
AddForceUpdaterPtr base_class_af_creator(SystemDefinitionPtr sysdef, std::shared_ptr<ParticleGroup> group, Scalar3 force_inc, Scalar3 domainlo, Scalar3 domainhi)
{
    return AddForceUpdaterPtr(new AddForceUpdater(sysdef, group, force_inc, domainlo, domainhi));
}

#ifdef ENABLE_CUDA
//! AddForceUpdater creator for add_force_basic_tests()
AddForceUpdaterPtr gpu_af_creator(SystemDefinitionPtr sysdef, std::shared_ptr<ParticleGroup> group, Scalar3 force_inc, Scalar3 domainlo, Scalar3 domainhi)
{
    return AddForceUpdaterPtr(new AddForceUpdaterGPU(sysdef, group, force_inc, domainlo, domainhi));
}
#endif

//! test case for add force on the CPU
UP_TEST( AddForceUpdater_basic )
{
    printf(" IN UP_TEST: CPU \n");
    addforce_creator af_creator = bind(base_class_af_creator, _1, _2, _3, _4, _5);
    add_force_basic_tests(af_creator, ExecutionConfigurationPtr(new ExecutionConfiguration(ExecutionConfiguration::CPU)));
}

#ifdef ENABLE_CUDA
//! test case for angle forces on the GPU
UP_TEST( AddForceUpdaterGPU_basic )
{
    printf(" IN UP_TEST: GPU \n");
    addforce_creator af_creator = bind(gpu_af_creator, _1, _2, _3, _4, _5);
    add_force_basic_tests(af_creator, ExecutionConfigurationPtr(new ExecutionConfiguration(ExecutionConfiguration::GPU)));
}

#endif
