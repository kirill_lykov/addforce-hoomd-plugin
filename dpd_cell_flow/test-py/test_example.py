# -*- coding: iso-8859-1 -*-
# Maintainer: joaander

from hoomd import *
from hoomd import md
context.initialize();
import hoomd.dpd_cell_flow;
import unittest;
import os;

# tests md.force.constant
class add_force_tests(unittest.TestCase):
    def setUp(self):
        print
        self.system = init.create_lattice(lattice.fcc(a=1.0),n=[10,5,5]);
        print ("Box dim %g %g %g"%(self.system.box.Lx, self.system.box.Ly, self.system.box.Lz))
        context.current.sorter.set_params(grid=8)

    def test_create(self):
        f = (0, 0.5, 0.0)
        box = self.system.box
        lo = (-box.Lx/2., -box.Ly/2., -box.Lz/2.)
        hi = ( box.Lx/2.,  box.Ly/2.,  box.Lz/2.)
        updater = hoomd.dpd_cell_flow.update.add_force(group.all(), f, lo, hi);

    def test_flow(self):
        # just to check it up, later will be transformed in something more meaningful
        nl = md.nlist.cell(r_buff=0.4, check_period=1)
        dpd = md.pair.dpd(r_cut=1.0, nlist = nl, kT=0.0945, seed=10);
        dpd.pair_coeff.set('A', 'A', A=10.0, gamma = 45.0, r_cut=1.0);
        dpd.update_coeffs();
        
        box = self.system.box
        lo = (-box.Lx/2., -box.Ly/2., -box.Lz/2.)
        hi = (0, box.Ly/2., box.Lz/2.)
        updaterLeft = hoomd.dpd_cell_flow.update.add_force(group.all(), (0, -1, 0), lo, hi);

        lo = (0, -box.Ly/2., -box.Lz/2.)
        hi = (box.Lx/2., box.Ly/2., box.Lz/2.)
        updaterLeft = hoomd.dpd_cell_flow.update.add_force(group.all(), (0, 1, 0), lo, hi);    

        md.integrate.mode_standard(dt=0.01)
        md.integrate.nve(group = group.all())

        dump.gsd(filename="traj.gsd", period=10, group=group.all(), phase=0, overwrite=True)
        run(1e3)

    def tearDown(self):
        context.initialize();


if __name__ == '__main__':
    unittest.main(argv = ['test_example.py', '-v'])
