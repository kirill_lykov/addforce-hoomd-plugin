// Copyright (c) 2009-2017 The Regents of the University of Michigan
// This file is part of the HOOMD-blue project, released under the BSD 3-Clause License.

// Include the defined classes that are to be exported to python
#include "AddForceUpdater.h"

#include <hoomd/extern/pybind/include/pybind11/pybind11.h>

// specify the python module. Note that the name must expliclty match the PROJECT() name provided in CMakeLists
// (with an underscore in front)
PYBIND11_PLUGIN(_dpd_cell_flow)
    {
    pybind11::module m("_dpd_cell_flow");
    export_AddForceUpdater(m);

    #ifdef ENABLE_CUDA
    export_AddForceUpdaterGPU(m);
    #endif

    return m.ptr();
    }
