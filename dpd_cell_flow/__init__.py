# Copyright (c) 2009-2017 The Regents of the University of Michigan
# This file is part of the HOOMD-blue project, released under the BSD 3-Clause License.

# this file exists to mark this directory as a python module

# need to import all submodules defined in this directory

# NOTE: adjust the import statement to match the name of the template
# (here: dpd_cell_flow)
from hoomd.dpd_cell_flow import update
