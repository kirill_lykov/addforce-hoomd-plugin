project(dpd_cell_flow)
CMAKE_MINIMUM_REQUIRED(VERSION 2.6.2 FATAL_ERROR)

set(CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_MODULE_PATH})
SET(CMAKE_INSTALL_RPATH "$ORIGIN/..:$ORIGIN")

include(FindHOOMD.cmake)

# plugins must be built as shared libraries
if (ENABLE_STATIC)
    message(SEND_ERROR "Plugins cannot be built against a statically compiled hoomd")
endif (ENABLE_STATIC)

set(BUILD_SHARED_LIBS on)

if(CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
  set(CMAKE_INSTALL_PREFIX ${HOOMD_ROOT} CACHE PATH "Installation prefix" FORCE)
endif(CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)

set(PYTHON_MODULE_BASE_DIR ${CMAKE_INSTALL_PREFIX})
message(STATUS "Install plugin to: " ${PYTHON_MODULE_BASE_DIR})

# CODE I COPIED FROM HOOMD/CMakeList.txt to introduce testing
set(cmake_files CMake/hoomd/HOOMDCFlagsSetup.cmake
                CMake/hoomd/HOOMDCommonLibsSetup.cmake
                CMake/hoomd/HOOMDCUDASetup.cmake
                CMake/hoomd/test.cc
                CMake/hoomd/HOOMDMacros.cmake
                CMake/hoomd/HOOMDMPISetup.cmake
                CMake/hoomd/HOOMDNumpySetup.cmake
                CMake/hoomd/HOOMDOSSpecificSetup.cmake
                CMake/hoomd/HOOMDPythonSetup.cmake
                CMake/git/FindGit.cmake
                CMake/git/GetGitRevisionDescription.cmake
                CMake/libgetar/Findlibgetar.cmake
                CMake/thrust/FindThrust.cmake
                CMake_build_options.cmake
                CMake_install_options.cmake
                CMake_preprocessor_flags.cmake
                CMake_src_setup.cmake
                CMake_version_options.cmake
   )

################################
# set up unit tests
enable_testing()
option(BUILD_TESTING "Build unit tests" ON)
if (BUILD_TESTING)
     # add test_all to the ALL target
     add_custom_target(test_all ALL)

endif (BUILD_TESTING)

# add subdirectories
add_subdirectory(${PROJECT_NAME})
